var webpack = require('webpack');
var path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, 'build');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
  plugins: [
    new CopyWebpackPlugin([{ from: path.resolve(__dirname, 'src/client/index.html'), to: BUILD_DIR }])
  ],
  module: {
    rules: [
    {
             test: /\.(png|svg|jpg|gif)$/,
             use: [
               'file-loader'
             ]
           },
      {
        test: /\.jsx?/,
        include: APP_DIR,
        loader: 'babel-loader'
      }
    ]
  },
  devServer: {
    contentBase: BUILD_DIR,
    proxy: {
      "/flixbusapi": {
        target: "http://api.sandbox.mfb.io",
        pathRewrite: {"^/flixbusapi" : ""},
        changeOrigin: true,
        headers: { 'X-API-Authentication': 'DEV_TEST_TOKEN_STAGING' }
      }
    }
  }
};

module.exports = config;
