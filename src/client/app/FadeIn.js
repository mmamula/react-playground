import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group';

class FadeIn extends React.Component {
  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        {this.props.children}
      </CSSTransitionGroup>
    );
  }
}

export default FadeIn;