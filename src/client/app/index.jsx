import React from 'react';
import { render } from 'react-dom';
import FadeIn from './FadeIn';


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          one: 0,
          two: 0,
          three: 0,
          four: 0,
        }
    }

    componentDidMount() {
      const timeout = 30;
      setInterval(() => {
        const key = Object.keys(this.state)[Math.floor(Math.random() * 4)]
        this.setState({ [key]: 1 });
        setTimeout(() => {
          this.setState({ [key]: 0 });
          setTimeout(() => {
            this.setState({ [key]: 1 });
            setTimeout(() => {
                this.setState({ [key]: 0 });
            }, timeout)
          }, timeout)
        }, timeout)
      }, 800)
    }

    render () {
        return (
          <div style={{ height: '100%' }}>
          <img style={{ display: this.state.one === 1 ? 'block' : 'none', opacity: this.state.one, position: 'fixed', top: -150, left: -150, transform: 'rotate(-45deg)' }} src={require('./Lightning.png')} />
          <img style={{ display: this.state.two === 1 ? 'block' : 'none', opacity: this.state.two, position: 'fixed', top: -150, right: -150, transform: 'rotate(45deg)' }} src={require('./Lightning.png')} />
          <img style={{ display: this.state.three === 1 ? 'block' : 'none', opacity: this.state.three, position: 'fixed', bottom: -150, right: -150, transform: 'rotate(135deg)' }} src={require('./Lightning.png')} />
          <img style={{ display: this.state.four === 1 ? 'block' : 'none', opacity: this.state.four, position: 'fixed', bottom: -150, left: -150, transform: 'rotate(135deg)' }} src={require('./Lightning.png')} />
              <FadeIn>
                <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', height: '100%' }}>
                  <div style={{ margin: '0 auto' }} >
                    <img src={require('./mm.jpg')} style={{ width: '100%', maxWidth: 400 }} />
                    <form className='demo-form'>
                      <input type='text' placeholder='Username' />
                      <input type='password' placeholder='Password' />
                      <button type='submit'>LogIn</button>
                    </form>
                  </div>
                </div>
              </FadeIn>
          </div>
        );
    }
}

render(<App />, document.getElementById('app'));
